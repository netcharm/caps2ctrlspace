﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caps2CtrlSpace
{
    public struct Rect
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }

    enum TernaryRasterOperations : uint
    {
        /// <summary>dest = source</summary>
        SRCCOPY = 0x00CC0020,
        /// <summary>dest = source OR dest</summary>
        SRCPAINT = 0x00EE0086,
        /// <summary>dest = source AND dest</summary>
        SRCAND = 0x008800C6,
        /// <summary>dest = source XOR dest</summary>
        SRCINVERT = 0x00660046,
        /// <summary>dest = source AND (NOT dest)</summary>
        SRCERASE = 0x00440328,
        /// <summary>dest = (NOT source)</summary>
        NOTSRCCOPY = 0x00330008,
        /// <summary>dest = (NOT src) AND (NOT dest)</summary>
        NOTSRCERASE = 0x001100A6,
        /// <summary>dest = (source AND pattern)</summary>
        MERGECOPY = 0x00C000CA,
        /// <summary>dest = (NOT source) OR dest</summary>
        MERGEPAINT = 0x00BB0226,
        /// <summary>dest = pattern</summary>
        PATCOPY = 0x00F00021,
        /// <summary>dest = DPSnoo</summary>
        PATPAINT = 0x00FB0A09,
        /// <summary>dest = pattern XOR dest</summary>
        PATINVERT = 0x005A0049,
        /// <summary>dest = (NOT dest)</summary>
        DSTINVERT = 0x00550009,
        /// <summary>dest = BLACK</summary>
        BLACKNESS = 0x00000042,
        /// <summary>dest = WHITE</summary>
        WHITENESS = 0x00FF0062,
        /// <summary>
        /// Capture window as seen on screen.  This includes layered windows
        /// such as WPF windows with AllowsTransparency="true"
        /// </summary>
        CAPTUREBLT = 0x40000000
    }

    public class ImeIndicator
    {
        private Bitmap image_layout = null;
        public Bitmap Layout
        {
            get { return (image_layout); }
            set { if (image_layout is Bitmap) image_layout.Dispose(); image_layout = null; image_layout = value; hash_layout = Ime.CalcHash(value); }
        }
        private Bitmap image_english = null;
        public Bitmap English
        {
            get { return (image_english); }
            set { if (image_english is Bitmap) image_english.Dispose(); image_english = null; image_english = value; hash_english = Ime.CalcHash(value); }
        }
        private Bitmap image_locale = null;
        public Bitmap Locale
        {
            get { return (image_locale); }
            set { if (image_locale is Bitmap) image_locale.Dispose(); image_locale = null; image_locale = value; hash_locale = Ime.CalcHash(value); }
        }
        private Bitmap image_disabled = null;
        public Bitmap Disabled
        {
            get { return (image_disabled); }
            set { if (image_disabled is Bitmap) image_disabled.Dispose(); image_disabled = null; image_disabled = value; hash_disabled = Ime.CalcHash(value); }
        }
        private Bitmap image_close = null;
        public Bitmap Close
        {
            get { return (image_close); }
            set { if (image_close is Bitmap) image_close.Dispose(); image_close = null; image_close = value; hash_close = Ime.CalcHash(value); }
        }
        private Bitmap image_none = null;
        public Bitmap None
        {
            get { return (image_none); }
            set { if (image_none is Bitmap) image_none.Dispose(); image_none = null; image_none = value; hash_none = Ime.CalcHash(value); }
        }

        private string hash_layout = string.Empty;
        public string LayoutHash { get { if (string.IsNullOrEmpty(hash_layout)) hash_layout = Ime.CalcHash(Layout); return (hash_layout); } }
        private string hash_english = string.Empty;
        public string EnglishHash { get { if (string.IsNullOrEmpty(hash_english)) hash_english = Ime.CalcHash(English); return (hash_english); } }
        private string hash_locale = string.Empty;
        public string LocaleHash { get { if (string.IsNullOrEmpty(hash_locale)) hash_locale = Ime.CalcHash(Locale); return (hash_locale); } }
        private string hash_disabled = string.Empty;
        public string DisabledHash { get { if (string.IsNullOrEmpty(hash_disabled)) hash_disabled = Ime.CalcHash(Disabled); return (hash_disabled); } }
        private string hash_close = string.Empty;
        public string CloseHash { get { if (string.IsNullOrEmpty(hash_close)) hash_close = Ime.CalcHash(Close); return (hash_close); } }
        private string hash_none = string.Empty;
        public string NoneHash { get { if (string.IsNullOrEmpty(hash_none)) hash_none = Ime.CalcHash(None); return (hash_none); } }

        public double Tolerance { get; set; } = 0.05;
        public double LastCompareValue { get; set; } = 0.0;
        public bool LastCompareResult { get; set; } = false;

        private bool CompareHash(Bitmap src, Bitmap dst, string hash, double tolerance = 0.05)
        {
            LastCompareValue = 0.0;
            LastCompareResult = false;
            if (src is Bitmap && dst is Bitmap)
            {
                var diff = 1.0;
                var hash_src = Ime.CalcHash(src);
                var hash_dst = string.IsNullOrEmpty(hash) ? Ime.CalcHash(dst) : hash;
                if (!string.IsNullOrEmpty(hash_src) && !string.IsNullOrEmpty(hash_dst) && hash_src.Length == hash_dst.Length)
                {
                    int count = 0;
                    for (var i = 0; i < hash_src.Length; i++)
                    {
                        if (hash_src[i] != hash_dst[i]) count++;
                    }
                    diff = (double)count / (double)hash_src.Length;
                }
                LastCompareValue = 1 - diff;
                LastCompareResult = diff < tolerance;
#if DEBUG
                    System.Diagnostics.Debug.WriteLine($"Compare Result: {LastCompareResult}, Compare Value: {LastCompareValue:P1}");
#endif
            }
            return (LastCompareResult);
        }

        public bool CompareHash(Bitmap src, ImeIndicatorMode mode, double tolerance = 0.05)
        {
            var result = false;
            if (src is Bitmap)
            {
                switch (mode)
                {
                    case ImeIndicatorMode.Layout: result = CompareHash(src, Layout, LayoutHash, tolerance); break;
                    case ImeIndicatorMode.Close: result = CompareHash(src, Close, CloseHash, tolerance); break;
                    case ImeIndicatorMode.Disabled: result = CompareHash(src, Disabled, DisabledHash, tolerance); break;
                    case ImeIndicatorMode.English: result = CompareHash(src, English, EnglishHash, tolerance); break;
                    case ImeIndicatorMode.Locale: result = CompareHash(src, Locale, LocaleHash, tolerance); break;
                    default: break;
                }                
            }
            return (result);
        }
    }

    public enum ImeBarState { System, Embed, Float };
    public enum ImeIndicatorMode { None=-1, Layout = 0, English, Locale, Disabled, Close };

    public enum ImeIndicatorModeN
    {
        Layout = 0,
        On = 1,
        Off = 2,
        Disable = 3,
        Hiragana = 4,
        Katakana = 5,
        KatakanaHalf = 6,
        AlphaFull = 7,
        Alpha = 8,
        HangulFull = 9,
        Hangul = 10,
        Close = 11,
        OnHalf = 12
    };

    public class Ime
    {
        private static string AppPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().EscapedCodeBase).Replace("file:\\", "");

        #region get current input window keyboard layout functions
        private delegate bool EnumWindowsProc(IntPtr hWnd, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr AttachThreadInput(IntPtr idAttach, IntPtr idAttachTo, bool fAttach);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetFocus();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetKeyboardLayout(uint thread);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern long GetKeyboardLayoutName(StringBuilder pwszKLID);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetShellWindow();

        [DllImport("user32.dll", EntryPoint = "FindWindow", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowRect(IntPtr hwnd, out Rect lpRect);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool PrintWindow(IntPtr hwnd, IntPtr hDC, uint nFlags);

        [DllImport("gdi32.dll", EntryPoint = "BitBlt", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(
            [In] IntPtr hdc,
            int nXDest, int nYDest, int nWidth, int nHeight,
            [In] IntPtr hdcSrc,
            int nXSrc, int nYSrc,
            TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll", EntryPoint = "StretchBlt", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool StretchBlt(
            [In] IntPtr hdc,
            int nXDest, int nYDest, int nWidth, int nHeight,
            [In] IntPtr hdcSrc,
            int nXSrc, int nYSrc, int nSrcWidth, int nSrcHeight,
            TernaryRasterOperations dwRop);

        [DllImport("msimg32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool TransparentBlt(
            [In] IntPtr hdcDest,
            int nXDest, int nYDest, int nWDest, int nHDest,
            [In] IntPtr hdcSrc,
            int nXSrc, int nYSrc, int nWSrc, int nHSrc,
            uint crTransparent);

        // 创建结构体用于返回捕获时间  
        [StructLayout(LayoutKind.Sequential)]
        struct LASTINPUTINFO
        {
            // 设置结构体块容量  
            [MarshalAs(UnmanagedType.U4)]
            public int cbSize;
            // 捕获的时间  
            [MarshalAs(UnmanagedType.U4)]
            public uint dwTime;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        #endregion

        //获取键盘和鼠标没有操作的时间  
        public static uint GetLastInputTime()
        {
            uint idleTime = 0;
            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = Marshal.SizeOf(lastInputInfo);
            lastInputInfo.dwTime = 0;

            uint envTicks = (uint)Environment.TickCount;

            if (GetLastInputInfo(ref lastInputInfo))
            {
                uint lastInputTick = lastInputInfo.dwTime;

                idleTime = envTicks - lastInputTick;
            }

            return ((idleTime > 0) ? (idleTime / 1000) : 0);

            //LASTINPUTINFO vLastInputInfo = new LASTINPUTINFO();
            //vLastInputInfo.cbSize = Marshal.SizeOf(vLastInputInfo);
            //// 捕获时间  
            //if (!GetLastInputInfo(ref vLastInputInfo))
            //    return 0;
            //else
            //    return Environment.TickCount - (long)vLastInputInfo.dwTime;
        }

        private const int KL_NAMELENGTH = 9;

        private static CultureInfo[] CultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);
        //CultureInfo[] CultureInfos2 = CultureInfo.GetCultures(CultureTypes.AllCultures);

        private static Dictionary<int, string> InstalledKeyboardLayout = new Dictionary<int, string>();
        //private static int lastKeyboardLayout = (int)SysKeyboardLayout.ENG;
        private static int lastKeyboardLayout = InputLanguage.DefaultInputLanguage.Culture.KeyboardLayoutId;

        private Tuple<int, int> GetFocusKeyboardLayout()
        {
            IntPtr activeWindowHandle = GetForegroundWindow();
            IntPtr activeWindowThread = GetWindowThreadProcessId(activeWindowHandle, IntPtr.Zero);
            //IntPtr thisWindowThread = GetWindowThreadProcessId(this.Handle, IntPtr.Zero);

            //AttachThreadInput(activeWindowThread, thisWindowThread, true);
            IntPtr focusedControlHandle = GetFocus();
            var kl = GetKeyboardLayout((uint)activeWindowThread.ToInt32()).ToInt32() & 0xFFFF;
            //AttachThreadInput(activeWindowThread, thisWindowThread, false);
            return (new Tuple<int, int>(focusedControlHandle.ToInt32(), kl));
        }

        private static Dictionary<int, ImeIndicator> ImeIndicators = new Dictionary<int, ImeIndicator>();

        private static Bitmap current_ime_mode_bitmap = null;
        public static Bitmap CurrentImeModeBitmap
        {
            get { return (current_ime_mode_bitmap); }
            set
            {
                if (value is Bitmap)
                {
                    if (current_ime_mode_bitmap == null)
                        current_ime_mode_bitmap = new Bitmap(value);
                    else
                    {
                        using (Graphics g = Graphics.FromImage(current_ime_mode_bitmap))
                        {
                            g.Clear(Color.Transparent);
                            g.DrawImage(value, new Rectangle(0, 0, value.Width, value.Height));
                        }
                    }
                }
                else
                {
                    current_ime_mode_bitmap = value;
                }
            }
        }

        private static Bitmap current_input_indicator_bitmap = null;
        public static Bitmap CurrentInputIndicatorBitmap
        {
            get { return (current_input_indicator_bitmap); }
            set
            {
                if (value is Bitmap)
                {
                    if (current_input_indicator_bitmap == null)
                        current_input_indicator_bitmap = new Bitmap(value);
                    else
                    {
                        using (Graphics g = Graphics.FromImage(current_input_indicator_bitmap))
                        {
                            g.Clear(Color.Transparent);
                            g.DrawImage(value, new Rectangle(0, 0, value.Width, value.Height));
                        }
                    }
                }
                else
                {
                    current_input_indicator_bitmap = value;
                }
            }
        }

        public static int KeyboardLayout
        {
            get
            {
                return (GetKeyboardLayout());
            }
        }

        public static string KeyboardLayoutName
        {
            get
            {
                if (InstalledKeyboardLayout.ContainsKey(lastKeyboardLayout))
                    return (InstalledKeyboardLayout[lastKeyboardLayout]);
                else
                {
                    var kl = GetConsoleKeyboardLayout();
                    string klv = string.Empty;
                    if (InstalledKeyboardLayout.ContainsKey(kl))
                    {
                        klv = InstalledKeyboardLayout[kl];
                        lastKeyboardLayout = kl;
                    }
                    return (klv);
                }
            }
        }

        public static ImeIndicatorMode Mode
        {
            get
            {
                return (GetImeMode(lastKeyboardLayout));
            }
        }

        public static void UpdateIndicators(Bitmap Bmp, ImeIndicatorMode IndicatorMode)
        {
            if (Bmp is Bitmap)
            {
                UpdateIndicators(Bmp, lastKeyboardLayout, IndicatorMode);
            }
        }

        public static void UpdateIndicators(Bitmap Bmp, int KeyboardLayout, ImeIndicatorMode IndicatorMode)
        {
            if (Bmp is Bitmap)
            {
                switch (IndicatorMode)
                {
                    case ImeIndicatorMode.Layout: ImeIndicators[KeyboardLayout].Layout = Bmp; break;
                    case ImeIndicatorMode.Close: ImeIndicators[KeyboardLayout].Close = Bmp; break;
                    case ImeIndicatorMode.Disabled: ImeIndicators[KeyboardLayout].Disabled = Bmp; break;
                    case ImeIndicatorMode.English: ImeIndicators[KeyboardLayout].English = Bmp; break;
                    case ImeIndicatorMode.Locale: ImeIndicators[KeyboardLayout].Locale = Bmp; break;
                    default: break;
                }
            }
        }

        public static double Tolerance { get; set; } = 0.05;

        private static double compare_result_value { get; set; } = 0.0;
        public static double CompareResultValue
        {
            get { return (ImeIndicators.ContainsKey(lastKeyboardLayout) ? ImeIndicators[lastKeyboardLayout].LastCompareValue : 0); }
        }
        public static bool CompareResult
        {
            get { return (ImeIndicators.ContainsKey(lastKeyboardLayout) ? ImeIndicators[lastKeyboardLayout].LastCompareResult : false); }
        }

        public static IntPtr ActiveWindowHandle { get; private set; } = IntPtr.Zero;
        public static string ActiveWindowTitle
        {
            get
            {
                string title = string.Empty;
                if (ActiveWindowHandle != IntPtr.Zero)
                {
                    int length = GetWindowTextLength(ActiveWindowHandle);
                    if (length > 0)
                    {
                        StringBuilder sb = new StringBuilder(length);
                        GetWindowText(ActiveWindowHandle, sb, length + 1);
                        title = sb.ToString();
                    }
                }
                return (title);
            }
        }

        public static Bitmap ToBW(Bitmap Bmp)
        {
            if (Bmp is Bitmap)
                return (Bmp.Clone(new Rectangle(0, 0, Bmp.Width, Bmp.Height), PixelFormat.Format4bppIndexed));
            else return null;
        }

        public static Bitmap ToGrayScale(Bitmap Bmp, out int GrayMean)
        {
            int rgb;
            Color c;
            int a = 255;
            byte[] grayValues = new byte[Bmp.Width * Bmp.Height];

            for (int y = 0; y < Bmp.Height; y++)
            {
                for (int x = 0; x < Bmp.Width; x++)
                {
                    c = Bmp.GetPixel(x, y);
                    a = (c.R == 0 && c.G == 0 && c.B == 0) ? 0 : c.A;
                    //a = (c.R < 0x80 && c.G < 0x80 && c.B < 0x80) ? 0 : c.A;
                    //a = (c.R < 0x40 && c.G < 0x40 && c.B < 0x40) ? 0 : c.A;
                    //Bmp.SetPixel(x, y, Color.FromArgb(a, c.R, c.G, c.B));

                    rgb = (int)Math.Round(.299 * c.R + .587 * c.G + .114 * c.B);
                    a = rgb < 0x80 ? 0 : c.A;
                    Bmp.SetPixel(x, y, Color.FromArgb(a, rgb, rgb, rgb));

                    byte gray = (byte)((c.R * 30 + c.G * 59 + c.B * 11) / 100);
                    grayValues[x * Bmp.Width + y] = gray;
                }
            }
            GrayMean = (int)(grayValues.ToList().Average( g => g));

            return ((Bitmap)Bmp.Clone());
        }

        public static Bitmap To8x8(Bitmap Bmp)
        {
            Bitmap result = null;
            if (Bmp is Bitmap)
            {
                Bitmap dst = new Bitmap(8, 8, PixelFormat.Format32bppArgb);
                using (Graphics gDst = Graphics.FromImage(dst))
                {
                    gDst.DrawImage(Bmp, new Rectangle(0, 0, dst.Width, dst.Height));
                }
                result = dst.Clone(new Rectangle(0, 0, dst.Width, dst.Height), PixelFormat.Format4bppIndexed);
                if (dst is Bitmap) dst.Dispose();
            }
            return (result);
        }

        public static byte ToGrayBytes8x8(Bitmap Bmp, out byte[] GrayValues)
        {
            if (Bmp is Bitmap)
            {
                GrayValues = new byte[64];
                Bitmap dst = new Bitmap(8, 8, PixelFormat.Format32bppArgb);
                using (Graphics gDst = Graphics.FromImage(dst))
                {
                    float thresh = -1 * ((float)127 / 255);
                    var cm_mono = new ColorMatrix(new float[][] {
                        new float[] {1.000F, 0.000F, 0.000F, 0.000F, 0.000F},
                        new float[] {0.000F, 1.000F, 0.000F, 0.000F, 0.000F},
                        new float[] {0.000F, 0.000F, 1.000F, 0.000F, 0.000F},
                        new float[] {0.000F, 0.000F, 0.000F, 1.000F, 0.000F},
                        new float[] {thresh, thresh, thresh, 0.000F, 1.000F},
                    });

                    var cm_y = new ColorMatrix( new[]{
                        new float[] {   0.2990f,   0.2990f,   0.2990f,   0.0000f,   0.0000f },        // red scaling factor
                        new float[] {   0.5870f,   0.5870f,   0.5870f,   0.0000f,   0.0000f },        // green scaling factor
                        new float[] {   0.1140f,   0.1140f,   0.1140f,   0.0000f,   0.0000f },        // blue scaling factor
                        new float[] {   0.0000f,   0.0000f,   0.0000f,   1.0000f,   0.0000f },        // alpha scaling factor
                        new float[] {   0.0000f,   0.0000f,   0.0000f,   0.0000f,   1.0000f }         // three translations
                    } );

                    ImageAttributes imageAttr = new ImageAttributes();
                    imageAttr.SetColorMatrix(cm_y);

                    gDst.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                    gDst.DrawImage(Bmp, new Rectangle(0, 0, 8, 8), 0, 0, Bmp.Width, Bmp.Height, GraphicsUnit.Pixel, imageAttr);
                }
                Color c;
                for (int y = 0; y < dst.Height; y++)
                {
                    for (int x = 0; x < dst.Width; x++)
                    {
                        c = dst.GetPixel(x, y);
                        byte gray = (byte)((c.R * 30 + c.G * 59 + c.B * 11) / 100);
                        GrayValues[x * dst.Width + y] = gray;
                    }
                }
                if (dst is Bitmap) dst.Dispose();
                return ((byte)(GrayValues.ToList().Average(g => g)));
            }
            else
            {
                GrayValues = null;
                return (0);
            }
        }

        public static int CalcGrayMean(Bitmap Bmp, out byte[] GrayValues)
        {
            var mean = 0;
            if (Bmp is Bitmap)
            {
                Color c;
                GrayValues = new byte[Bmp.Width * Bmp.Height];

                for (int y = 0; y < Bmp.Height; y++)
                {
                    for (int x = 0; x < Bmp.Width; x++)
                    {
                        c = Bmp.GetPixel(x, y);
                        byte gray = (byte)((c.R * 30 + c.G * 59 + c.B * 11) / 100);
                        GrayValues[x * Bmp.Width + y] = gray;
                    }
                }
                mean = (int)(GrayValues.ToList().Average(g => g));
            }
            else GrayValues = null;
            return (mean);
        }

        public static string CalcHash(Bitmap Bmp)
        {
            var hash = string.Empty;
            if (Bmp is Bitmap)
            {
                var grays = new byte[0];
                var mean = ToGrayBytes8x8(Bmp, out grays);
                char[] grayValues = new char[grays.Length];
                if (grays is byte[])
                {
                    grayValues = grays.Select(c => c < mean ? '0' : '1').ToArray();
                    hash = string.Concat(grayValues);
                }
            }
            return (hash);
        }

        public static double CalcSimilar(Bitmap src, Bitmap dst)
        {
            var similar = 0.0;

            if (src is Bitmap && dst is Bitmap)
            {
                var hash_src = CalcHash(src);
                var hash_dst = CalcHash(dst);
                if (!string.IsNullOrEmpty(hash_src) && !string.IsNullOrEmpty(hash_dst) && hash_src.Length == hash_dst.Length)
                {
                    int count = 0;                   
                    for (var i = 0; i < hash_src.Length; i++)
                    {
                        if (hash_src[i] != hash_dst[i]) count++;
                    }
                    similar = 1.0 - ((double)count / (double)hash_src.Length);
                }
            }
            compare_result_value = similar;

            return (similar);
        }

        public static bool CalcSimilar(Bitmap src, Bitmap dst, double tolerance = 0.05)
        {
            return (CalcSimilar(src, dst) >= 1 - tolerance);
        }

        private static void InitImeIndicatorsList()
        {
            InputLanguageCollection ilc = InputLanguage.InstalledInputLanguages; //获取所有安装的输入法
            foreach (InputLanguage il in ilc)
            {
                var ckl = il.Culture.KeyboardLayoutId;
                if (!InstalledKeyboardLayout.ContainsKey(ckl))
                    InstalledKeyboardLayout.Add(ckl, il.LayoutName);

                if (!ImeIndicators.ContainsKey(ckl))
                {
                    ImeIndicators[ckl] = new ImeIndicator();
                    ImeIndicators[ckl].Layout = ToBW(LoadBitmap(Path.Combine(AppPath, $"{ckl}_{(int)ImeIndicatorMode.Layout}.png")));
                    ImeIndicators[ckl].English = ToBW(LoadBitmap(Path.Combine(AppPath, $"{ckl}_{(int)ImeIndicatorMode.English}.png")));
                    ImeIndicators[ckl].Locale = ToBW(LoadBitmap(Path.Combine(AppPath, $"{ckl}_{(int)ImeIndicatorMode.Locale}.png")));
                    ImeIndicators[ckl].Disabled = ToBW(LoadBitmap(Path.Combine(AppPath, $"{ckl}_{(int)ImeIndicatorMode.Disabled}.png")));
                    ImeIndicators[ckl].Close = ToBW(LoadBitmap(Path.Combine(AppPath, $"{ckl}_{(int)ImeIndicatorMode.Close}.png")));
                }
            }
        }

        public static IntPtr GetImeModeButtonHandle()
        {
            IntPtr result = IntPtr.Zero;

            IntPtr hWnd = FindWindow("Shell_TrayWnd", null);
            if (hWnd != IntPtr.Zero)
            {
                IntPtr hTray = FindWindowEx(hWnd, IntPtr.Zero, "TrayNotifyWnd", null);
                if (hTray != IntPtr.Zero)
                {
                    IntPtr hInput = FindWindowEx(hTray, IntPtr.Zero, "TrayInputIndicatorWClass", null);
                    if (hInput != IntPtr.Zero && IsWindowVisible(hInput))
                    {
                        IntPtr hIme = FindWindowEx(hInput, IntPtr.Zero, "IMEModeButton", null);
                        result = hIme;
                        if (hIme != IntPtr.Zero) Marshal.Release(hIme);
                    }
                    Marshal.Release(hTray);
                }
                Marshal.Release(hWnd);
            }

            if (result == IntPtr.Zero)
            {
#if DEBUG
                IntPtr hInput = FindWindowEx(IntPtr.Zero, IntPtr.Zero, "CiceroUIWndFrame", "TF_FloatingLangBar_WndTitle");
                //Console.WriteLine(hInput);
#endif
            }

            return (result);
        }

        public static IntPtr GetInputIndicatorButtonHandle()
        {
            IntPtr result = IntPtr.Zero;
            IntPtr hWnd = FindWindow("Shell_TrayWnd", null);
            if (hWnd != IntPtr.Zero)
            {
                IntPtr hTray = FindWindowEx(hWnd, IntPtr.Zero, "TrayNotifyWnd", null);
                if (hTray != IntPtr.Zero)
                {
                    IntPtr hInput = FindWindowEx(hTray, IntPtr.Zero, "TrayInputIndicatorWClass", null);
                    if (hInput != IntPtr.Zero && IsWindowVisible(hInput))
                    {
                        IntPtr hIme = FindWindowEx(hInput, IntPtr.Zero, "InputIndicatorButton", null);
                        result = hIme;
                        if (hIme != IntPtr.Zero) Marshal.Release(hIme);
                    }
                    Marshal.Release(hTray);
                }
                Marshal.Release(hWnd);
            }
            return (result);
        }

        public static Bitmap GetSanpshot(IntPtr hWnd)
        {
            Bitmap result = null;
            if (hWnd != IntPtr.Zero)
            {
                Rect rect = new Rect();
                GetWindowRect(hWnd, out rect);

                var w = rect.Right - rect.Left;
                var h = rect.Bottom - rect.Top;
                if (w > 0 && h > 0)
                {
                    try
                    {
                        Bitmap dst = new Bitmap(w, h, PixelFormat.Format32bppArgb);
                        using (Graphics gDst = Graphics.FromImage(dst))
                        {
                            using (Graphics gSrc = Graphics.FromHwnd(hWnd))
                            {
                                //gDst.FillRectangle(Brushes.Transparent, 0, 0, w, h);
                                IntPtr hdcSrc = IntPtr.Zero;
                                IntPtr hdcDst = IntPtr.Zero;
                                try
                                {
                                    hdcSrc = gSrc.GetHdc();
                                    hdcDst = gDst.GetHdc();
                                    bool succeeded = BitBlt(hdcDst, 0, 0, w, h, hdcSrc, 0, 0, TernaryRasterOperations.SRCCOPY);
                                    //bool succeeded = TransparentBlt(hdcDst, 0, 0, w, h, hdcSrc, 0, 0, w, h, (uint)Color.Black.ToArgb());
                                    if (succeeded) result = (Bitmap)dst.Clone();
                                }
                                catch { }
                                finally
                                {
                                    if (hdcSrc != IntPtr.Zero) gSrc.ReleaseHdc(hdcSrc);
                                    if (hdcDst != IntPtr.Zero) gDst.ReleaseHdc(hdcSrc);
                                }
                            }
                        }
                        if (dst is Bitmap) dst.Dispose();
                    }
                    catch { }
                }
            }
            return (result);
        }

        public static Bitmap GetSanpshot8x8(IntPtr hWnd)
        {
            Bitmap result = null;

            if (hWnd != IntPtr.Zero)
            {
                Rect rect = new Rect();
                GetWindowRect(hWnd, out rect);

                var w = rect.Right - rect.Left;
                var h = rect.Bottom - rect.Top;
                if (w > 0 && h > 0)
                {
                    try
                    {
                        Bitmap dst = new Bitmap(8, 8, PixelFormat.Format32bppArgb);
                        using (Graphics gDst = Graphics.FromImage(dst))
                        {
                            using (Graphics gSrc = Graphics.FromHwnd(hWnd))
                            {
                                //gDst.FillRectangle(Brushes.Transparent, 0, 0, w, h);
                                IntPtr hdcSrc = IntPtr.Zero;
                                IntPtr hdcDst = IntPtr.Zero;
                                try
                                {
                                    hdcSrc = gSrc.GetHdc();
                                    hdcDst = gDst.GetHdc();
                                    //bool succeeded = BitBlt(hdcDst, 0, 0, w, h, hdcSrc, 0, 0, TernaryRasterOperations.SRCCOPY);
                                    bool succeeded = StretchBlt(hdcDst, 0, 0, 8, 8, hdcSrc, 0, 0, w, h, TernaryRasterOperations.SRCCOPY);
                                    if (succeeded) result = (Bitmap)dst.Clone();
                                }
                                catch { }
                                finally
                                {
                                    if (hdcSrc != IntPtr.Zero) gSrc.ReleaseHdc(hdcSrc);
                                    if (hdcDst != IntPtr.Zero) gDst.ReleaseHdc(hdcSrc);
                                }
                            }
                        }
                    }
                    catch { }
                }
            }

            return (result);
        }

        public static Bitmap GetImeModeBitmap()
        {
            Bitmap result = null;
            var hWnd = GetImeModeButtonHandle();
            if (hWnd != IntPtr.Zero)
            {
                result = GetSanpshot(hWnd);
                Marshal.Release(hWnd);
            }
            return (result);
        }

        public static Bitmap GetInputIndicatorBitmap()
        {
            Bitmap result = null;
            var hWnd = GetInputIndicatorButtonHandle();
            if (hWnd != IntPtr.Zero)
            {
                result = GetSanpshot(hWnd);
                Marshal.Release(hWnd);
            }
            return (result);
        }

        public static Bitmap LoadBitmap(string bitmapFile)
        {
            Bitmap result = null;
            if (File.Exists(bitmapFile))
            {
                using (var fs = new FileStream(bitmapFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var bmp = Image.FromStream(fs);
                    result = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format32bppArgb);
                    using (Graphics g = Graphics.FromImage(result))
                    {
                        g.DrawImage(bmp, 0, 0);
                    }
                    if (bmp is Image) bmp.Dispose();
                }
            }
            return (result);
        }

        public static bool CompareBitmap(Bitmap src, Bitmap dst, double tolerance = 0.05)
        {
            bool result = false;

            if (src is Bitmap && dst is Bitmap)
            {
                if (src.Width == dst.Width && src.Height == dst.Height)
                {
                    //result = true;
                    int count = 0;

                    //var bSrc = ToGrayScale(src);
                    //var bDst = ToGrayScale(dst);

                    var bSrc = ToBW(src);
                    //var bDst = ToBW(dst);
                    var bDst = dst;

                    Color cSrc, cDst;

                    for (int y = 6; y < bSrc.Height - 6; y++)
                    {
                        for (int x = 2; x < bSrc.Width - 2; x++)
                        {
                            cSrc = bSrc.GetPixel(x, y);
                            cDst = bDst.GetPixel(x, y);
                            if (cSrc.A == 0 && cDst.A == 0) continue;
                            if (cSrc.R != cDst.R || cSrc.G != cDst.G || cSrc.B != cDst.B)
                            {
                                //result = false;
                                //break;
                                count++;
                            }
                        }
                    }
                    if ((double)count / (src.Width * src.Height) < tolerance) result = true;
                }
            }
            return (result);
        }

        public static bool CompareBitmapHash(Bitmap src, Bitmap dst, double tolerance = 0.05)
        {
            return (CalcSimilar(src, dst, tolerance));
        }

        private static int GetConsoleKeyboardLayout()
        {
            int kl = 0;

            if (InstalledKeyboardLayout.Count <= 0)
            {
                InitImeIndicatorsList();
            }

            CurrentInputIndicatorBitmap = GetInputIndicatorBitmap();
            if (CurrentInputIndicatorBitmap is Bitmap)
            {
                InputLanguageCollection ilc = InputLanguage.InstalledInputLanguages; //获取所有安装的输入法
                foreach (InputLanguage il in ilc)
                {
                    var ckl = il.Culture.KeyboardLayoutId;
                    if (ImeIndicators.ContainsKey(ckl) && ImeIndicators[ckl].Layout is Bitmap)
                    {
                        if (ImeIndicators[ckl].CompareHash(CurrentInputIndicatorBitmap, ImeIndicatorMode.Layout, Tolerance))
                        {
                            kl = ckl;
                            break;
                        }
                    }
                }
            }

            return (kl);
        }

        private static int GetKeyboardLayout()
        {
            int result = 0;

            if (InstalledKeyboardLayout.Count <= 0)
            {
                InitImeIndicatorsList();
            }

            //var klt = GetFocusKeyboardLayout();
            IntPtr activeWindowHandle = GetForegroundWindow();
            IntPtr activeWindowThread = GetWindowThreadProcessId(activeWindowHandle, IntPtr.Zero);
            var kl = GetKeyboardLayout((uint)activeWindowThread.ToInt32()).ToInt32() & 0xFFFF;

            if (kl >= 0)
            {
                var klo = kl;
                if (kl == 0)
                    kl = GetConsoleKeyboardLayout();
#if DEBUG
                //Console.WriteLine($"{activeWindowHandle}:{activeWindowThread}, - {klo}:{kl}, {Control.IsKeyLocked(Keys.CapsLock)}");
#endif
                if (KeyMapper.CapsLockLightAutoCheck && KeyMapper.CurrentKeyboardLayout != SysKeyboardLayout.ENG && Control.IsKeyLocked(Keys.CapsLock))
                {
                    KeyMapper.ToggleCapsLock();
                }

                result = kl;
                lastKeyboardLayout = kl;
                ActiveWindowHandle = activeWindowHandle;
            }
            return (result);
        }

        private static ImeIndicatorMode GetImeMode(int KeyboardLayout)
        {
            ImeIndicatorMode result = ImeIndicatorMode.Disabled;
            
            CurrentInputIndicatorBitmap = GetInputIndicatorBitmap();
            CurrentImeModeBitmap = GetImeModeBitmap();
            if (CurrentImeModeBitmap is Bitmap)
            {
                var kl = KeyboardLayout;
                if (ImeIndicators.ContainsKey(kl) && ImeIndicators[kl].Locale is Bitmap)
                {
                    if(ImeIndicators[kl].CompareHash(CurrentImeModeBitmap, ImeIndicatorMode.Close, Tolerance))
                        result = ImeIndicatorMode.Close;
                    else if (ImeIndicators[kl].CompareHash(CurrentImeModeBitmap, ImeIndicatorMode.Locale, Tolerance))
                        result = ImeIndicatorMode.Locale;
                    else if (ImeIndicators[kl].CompareHash(CurrentImeModeBitmap, ImeIndicatorMode.English, Tolerance))
                        result = ImeIndicatorMode.English;
                }
            }

            return (result);
        }
    }
}
